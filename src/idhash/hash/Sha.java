package idhash.hash;

import idhash.main.Exec;

/**
 *
 * This class includes mostly void-methods to compare sort of sha hash.
 *
 * @version 08.02.2012
 * @author agilob
 */
public class Sha {
    
    public Exec exec = new Exec();
    

    /**
     * Method checks if the given hash is only hex-digits.
     *
     * @param hash A string to see if it is only hex-digits string.
     */
    private boolean standard(String hash) {

        boolean amITrue = false;
        amITrue = hash.matches("[0-9a-f]{40,64}");

        return amITrue;

    }

    /**
     * Method checks if the given hash is clean SHA1 hash.
     *
     * @param hash A string received to check if it is clean SHA1.
     */
    private void csha(String hash) {

        boolean imTesting = false;
        imTesting = standard(hash);

        if (hash.length() == 40 && imTesting == true) {
            exec.results.add("Maybe - SHA1");
        }
    }

    /**
     * Method checks if the given hash is clean SHA224 hash.
     *
     * @param hash A string received to check if it is clean SHA-224.
     */
    private void sha224(String hash) {

        boolean imTesting = false;
        imTesting = standard(hash);

        if (hash.length() == 56 && imTesting == true) {
            exec.results.add("SHA224");
        }
    }

    /**
     * Method checks if the given hash is clean SHA256 hash.
     *
     * @param hash A string received to check if it is clean SHA-256.
     */
    private void sha256(String hash) {

        boolean imTesting = false;
        imTesting = standard(hash);

        if (hash.length() == 64 && imTesting == true) {
            exec.results.add("Maybe - SHA256");
        }

    }

    /**
     * Method checks if the given hash is SMF1.1.x or SMF2 hash.
     *
     * @param hash A string received to check if it is SMF1.1.x or SMF2 hash.
     */
    private void smf2(String hash) {

        int colon = hash.length() - 40;
        if (hash.charAt(colon) == ':') {
            exec.results.add("SMF1.1.x or SMF2.*"
                    + "\t" + "sha1(Username.Password)");
        }

    }

    /**
     * Check'em all!!
     *
     * @param mainHash A string object received from Main class, checking by
     * every SHA* method in this class.
     * @param dataFromInput A string object received from Main class, checking
     * by smf() method.
     */
    public void checkit(String mainHash, String dataFromInput) {
        csha(mainHash);
        sha224(mainHash);
        sha256(mainHash);
        smf2(dataFromInput);
    }
}
package idhash.hash;

import idhash.main.Exec;

/**
 * Class includes methods to find proper hash algorithm when is different than
 * md5 and sha.
 *
 * @version 05.02.2012
 * @author agilob
 */
public class Others {
    
    public Exec exec = new Exec();

    private boolean standard(String string) {

        boolean amITrue = false;
        amITrue = string.matches("[0-9a-f]{4,80}");

        return amITrue;

    }

    /**
     * Methods checks if the hash is 4 hex-digit hash.
     *
     * @param hash
     */
    private void crc16(String hash) { //which is hash

        boolean imTesting = false;
        imTesting = standard(hash);
        if (hash.length() == 4 && imTesting == true) {
            exec.results.add("CRC16");
        }
    }

    /**
     * Methods checks if the hash is a 8 digit hash.
     *
     * @param hash
     */
    private void crc32(String hash) { //which is hash
        boolean imTesting = false;
        imTesting = standard(hash);
        if (hash.length() == 4 && imTesting == false) {
            exec.results.add("CRC32");
        }
    }

    /**
     * Methods checks if the hash is a 40 hex-digit hash.
     *
     * @param hash
     */
    private void ripmed160(String hash) { //which is hash

        boolean imTesting = false;
        imTesting = standard(hash);

        if (hash.length() == 40 && imTesting == true) {
            exec.results.add("Maybe - Ripmed-160");
        }
    }

    /**
     * Methods checks if the hash is 8 digit hash.
     *
     * @param hash
     */
    private void ripmed256(String hash) { //which is hash

        boolean imTesting = false;
        imTesting = standard(hash);

        if (hash.length() == 64 && imTesting == true) {
            exec.results.add("Maybe - Ripmed-256");
        }
    }

    /**
     * Methods checks if the hash is a 80 hex-digit hash.
     *
     * @param hash
     */
    private void ripmed320(String hash) { //which is hash

        boolean imTesting = false;
        imTesting = standard(hash);

        if (hash.length() == 80 && imTesting == true) {
            exec.results.add("Maybe - Ripmed-320");
        }
    }

    /**
     * Methods checks if the hash is a 13 digit hash.
     *
     * @param hash
     */
    private void des(String hash) { //which is hash

        if (hash.length() == 13) {
            exec.results.add("DES");
        }
    }

    /**
     * Methods checks if the hash is a 40 hex-digit hash.
     *
     * @param hash
     */
    private void mysql5(String hash) { //hash

        boolean starts = false;
        int longe = 0;
        boolean imTesting = false;

        starts = hash.startsWith("*");
        longe = hash.length();
        imTesting = standard(hash);

        if (starts == true || (longe == 40 && imTesting == true)) {
            exec.results.add("Maybe - MYSQL5");
        }
    }

    /**
     * Methods checks if the hash is a 16 hex-digit hash.
     *
     * @param hash
     */
    private void mysql3(String hash) {

        boolean imTesting = false;
        imTesting = standard(hash);

        if (hash.length() == 16 && imTesting == true) {
            exec.results.add("MySQL323");
        }
    }

    /**
     * Methods checks if the hash is a 128 hex-digit hash.
     *
     * @param hash
     */
    private void whirlpool(String hash) {

        if (hash.length() == 128) {
            exec.results.add("Whirlpool");
        }
    }

    /**
     * Check'em all!!
     *
     * @param mainHash
     * @param dataFromInput
     */
    public void checkit(String mainHash, String dataFromInput) {

        des(dataFromInput); // KYAL96iSSXyNE
        mysql5(mainHash); // 950e92dc846efb193ba27aa634d77c57996ed108
        mysql3(mainHash); // 4000cfba4bc21a6c
        whirlpool(mainHash); // 31b92713a95f97756acd4569357e835e014f6fe9a3ed76d93fea6fcb61c4ec3b928520414d1e5ff0402b1cc0c6168bbd676caabdeb217b57c731550ff975b1ae
        ripmed320(mainHash); // 8106e323ef57becfdf631071ee1ba348b50630c1745f1cbe5ad700649327c6be7a8ce25d7161b2ad
        ripmed256(mainHash); // 7c129f373281f29cc3f4e3ad969ecfcd580da37e5058bc03fa57d0e7384a3818
        ripmed160(mainHash); // edf5342e4ba90bc55bcedbb52cb505c1a35fc62c
        crc16(mainHash); // f70b
        crc32(mainHash); // ripm
    }
}
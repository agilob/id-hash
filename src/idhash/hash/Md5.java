package idhash.hash;

import idhash.main.Exec;

/**
 * This class includes mostly void-methods to compare some sorts of MD5 hashes.
 *
 * @version 08.02.2012
 * @author agilob
 */
public class Md5 {

   /**
    * exec
    */
   public Exec exec = new Exec();

   /**
    * Method checks if the hash is really _clean_ MD5.
    *
    * @param hash Method is used often, checking if the hash is 32 hex-digits.
    */
   private boolean standard(String hash) {

      boolean amITrue = false;
      amITrue = hash.matches("[0-9a-f]{32}");

      return amITrue;

   }

   /**
    * Methods prints clue about hash when standard() returns true.
    *
    * @param hash String is checked to see if it is clean 32 hex-digit hash.
    */
   private void cmd5(String hash) {

      if (standard(hash) == true) {
         exec.results.add("This should me one of those:\n \t MD5(), MD5(MD5), NTLM(), MD4(), DCC()...");
      }

   }

   /**
    * Methods checks if the hash is osCommerce algorithm. Based on
    * String.matches(), checks how does the salt look like and takes boolean
    * value from standard().
    *
    * @param hash To check if the hash is a 32 hex-digit hash.
    * @param salt To check if the salt if really from osCommerce.
    */
   private void oscommerce(String hash, String salt) {

      boolean amITrue = false;
      amITrue = standard(hash);

      if (salt.matches("[0-9a-f]{2}") == true && amITrue == true) {
         exec.results.add("osCommerce"
                 + "\t" + "md5(Salt.Password)");
      }

   }

   /**
    * Method checks if the hash is from Moodle. Checking based on salt.length()
    * and a boolean parameter from standard().
    *
    * @param hash To check if the hash is a 32 hex-digit hash.
    * @param salt Let's see if the salt is 30 chars length
    */
   private void moodle(String hash, String salt) {

      boolean amITrue = false;
      amITrue = standard(hash);

      if (salt.length() == 30 && amITrue == true) {
         exec.results.add("Maybe - Moodle"
                 + "\t" + "md5(Password.Salt)");
      }

   }

   /**
    * Method checks if the hash is from IPB CMS. Checking based on
    * salt.length() and a boolean parameter from standard().
    *
    * @param hash To check if the hash is a 32 hex-digit hash.
    * @param salt To check if the salt if really from IPB.
    */
   private void ipb(String hash, String salt) {

      boolean amITrue = false;
      amITrue = standard(hash);

      if (salt.length() == 5 && amITrue == true) {
         exec.results.add("IPB2 or IPB3"
                 + "\t" + "md5(md5(Password).Salt)");
      }

   }

   /**
    * Method checks if the hash if from MD5(WordPress). Based on hash build.
    *
    * @param hash Checking based on hash structure.
    */
   private void wp(String hash) {

      boolean check = false;

      check = hash.startsWith("$P$B");
      if (check) {
         exec.results.add("WordPress"
                 + "\t" + "md5(Salt.Hash)");
      }
      //return check;
   }

   /**
    * Method checks if the hash if from MD5(phpbb3). Based on hash build.
    *
    * @param hash Checking based on hash structure.
    */
   private void phpbb3(String hash) {

      boolean check = false;

      check = hash.startsWith("$H$9");
      if (check) {
         exec.results.add("PHPBB3"
                 + "\t" + "md5(Salt.Hash)");
      }
   }

   /**
    * Method checks if the hash is from vBulettin 3.x.x. Based on salt
    * structure and length.
    *
    * @param hash To check if the hash is a 32 hex-digit hash.
    * @param salt To check if the salt if really from vB 3.x.x.
    */
   private void vb3(String hash, String salt) {

      boolean amITrue = false;
      amITrue = standard(hash);
      if (salt.length() == 3 && amITrue == true) {
         exec.results.add("vBulletin3"
                 + "\t" + "md5(md5(Password).Salt)");
      }

   }

   /**
    * Method checks if the hash is from vBulettin 4.x.x. Based on salt
    * structure and length
    *
    * @param hash To check if the hash is a 32 hex-digit hash.
    * @param salt To check if the salt if really from vB 4.x.x.
    */
   private void vb4(String hash, String salt) {
      boolean amITrue = false;
      amITrue = standard(hash);
      if (salt.length() == 30 && amITrue == true) {
         exec.results.add("Maybe - vBulletin4"
                 + "\t" + "md5(md5(Password).Salt)");
      }

   }

   /**
    * Method checks if the hash is from Joomla. Based on salt structure and
    * length
    *
    * @param hash To check if the hash is a 32 hex-digit hash.
    * @param salt To check if the salt if really from Joomla.
    */
   private void joomla15(String hash, String salt) {
      boolean amITrue = false;
      amITrue = standard(hash);
      if (salt.length() == 32 && amITrue == true) {
         exec.results.add("Joomla 1.5"
                 + "\t" + "md5(Password.Salt)");
      }

   }

   /**
    * Method checks if the hash if from MD5(UNIX). Based on hash build.
    *
    * @param hash Checking based on hash structure.
    */
   private void unix(String hash) {
      boolean starts = false;
      boolean ends = false;

      starts = hash.startsWith("$1$");

      if (!ends) {
         ends = hash.endsWith("0");
      }

      if (!ends) {
         ends = hash.endsWith("1");
      }

      if (!ends) {
         ends = hash.endsWith(".");
      }

      if (!ends) {
         ends = hash.endsWith("/");
      }

      if (starts && ends) {
         exec.results.add("MD5(Unix)"
                 + " \t" + "md5(Salt.Password)");
      }
   }

   /**
    * Puts received data to every method in this class to find hashing
    * algorithm.
    *
    * @param mainHash
    * @param mainSalt
    */
   public void check(String mainHash, String mainSalt) {
      cmd5(mainHash);    // 5cf5c8b71b27a3fd06148e8adc8ce550
      wp(mainHash);      // $P$B.vSW36Tfx.I6m4S/vifkY5BoMI14/1
      phpbb3(mainHash); // $H$9L/IRkrR48I5Jz78V2zd2hUNUjXa1g0
      vb3(mainHash, mainSalt); // 5cf5c8b71b27a3fd06148e8adc8ce550:f%y 
      vb4(mainHash, mainSalt); // e83d65901c97abb530797f05a8e53663:o:A25uH,7`#X04=v5,F/:{O-*}|M8>
      joomla15(mainHash, mainSalt); // 490b90ae9405f3da0f41179e090f4e0a:QmZ1TPuQRZ7Y29wLkaGIOlM4x1cMx9DE
      unix(mainHash); // $1$F4ZZsyXf$fvUZLqGv8RVYX3jT20hJn0 
      moodle(mainHash, mainSalt); // bc61347cc7bdbb80839eae94aeda8f6b:MEGxl;>^CG/a,S=ZH}p*fLL4#6)f2q
      oscommerce(mainHash, mainSalt);
      ipb(mainHash, mainSalt); // 0c41f8ffa19201f4231aa3778368a2e1:HskCG   

   }
}